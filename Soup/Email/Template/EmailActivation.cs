﻿namespace Soup.Email.Template
{
    public class EmailActivation
    {
        public string? Email { get; set; }
        public string? Link { get; set; }
        public string? Text { get; set; }
    }
}
