﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Soup.Data;
using Soup.DTOs;
using Soup.Models;

namespace Soup.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly UserData _userData;
        private readonly IConfiguration _configuration;
        private readonly PaymentData _paymentData;

        public AdminController(UserData userData, IConfiguration configuration, PaymentData paymentData)
        {
            _userData = userData;
            _configuration = configuration;
            _paymentData = paymentData;
        }

        [HttpPut("TogleStatusUser")]
        [Authorize(Roles = "Admin")]
        public IActionResult TogleStatusUser([FromQuery] Guid id, [FromQuery] string email)
        {
            try
            {
                User? user = _userData.CheckUser(email);

                if (user is null) return BadRequest("User Not Found");

                bool result = user.Status ? _userData.DeactivateUser(id) : _userData.ActivateUser(id);

                if (result)
                {
                    return Ok("User Activated Successfully");
                }
                return Ok("User Deactivated Succesfully");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetAllUsersByName")]

        public IActionResult GetAllUsersByName()
        {
            try
            {
                List<User> users = _userData.GetAllUserFromName();

                List<object> result = new List<object>();

                foreach (User user in users)
                {
                    result.Add(new
                    {
                        Id = user.Id,
                        Username = user.Username,
                        Email = user.Email,
                        Role = user.Role,
                        Status = user.Status
                    });
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetAllUsersByEmail")]

        public IActionResult GetAllUsersByEmail()
        {
            try
            {
                List<User> users = _userData.GetAllUserFromEmail();

                List<object> result = new List<object>();

                foreach (User user in users)
                {
                    result.Add(new
                    {
                        Id = user.Id,
                        Username = user.Username,
                        Email = user.Email,
                        Role = user.Role,
                        Status = user.Status
                    });
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetAllUsersByRole")]

        public IActionResult GetAllUsersByRole()
        {
            try
            {
                List<User> users = _userData.GetAllUserFromRole();

                List<object> result = new List<object>();

                foreach (User user in users)
                {
                    result.Add(new
                    {
                        Id = user.Id,
                        Username = user.Username,
                        Email = user.Email,
                        Role = user.Role,
                        Status = user.Status
                    });
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetAllUsersByStatus")]

        public IActionResult GetAllUsersByStatus()
        {
            try
            {
                List<User> users = _userData.GetAllUserByStatus();

                List<object> result = new List<object>();

                foreach (User user in users)
                {
                    result.Add(new
                    {
                        Id = user.Id,
                        Username = user.Username,
                        Email = user.Email,
                        Role = user.Role,
                        Status = user.Status
                    });
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
        [HttpGet("GetAllUsers")]
        
        public IActionResult GetAllUsers()
        {
            try
            {
                List<User> users = _userData.GetAll();

                List<object> result = new List<object>();

                foreach (User user in users)
                {
                    result.Add(new
                    {
                        Id = user.Id,
                        Username = user.Username,
                        Email = user.Email,
                        Role = user.Role,
                        Status = user.Status
                    });
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPut("UpdateUser")]
        [Authorize(Roles = "Admin")]
        public IActionResult UpdateUser([FromQuery] Guid id, [FromQuery] string email, [FromBody] UserDto userDto)
        {
            try
            {
                User user = new User
                {
                    Username = userDto.Username,
                    Email = userDto.Email,
                    Role = userDto.Role
                };

                User? checkUser = _userData.CheckUser(email);

                if (checkUser is null) return BadRequest("User Not Found");

                bool result = _userData.UpdateUserAccount(id, user);

                if (result)
                {
                    return Ok("User Has Been Updated Successfully");
                }
                else
                {
                    return StatusCode(500, "Updating User Failed");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("CreatePayment")]
        [Authorize(Roles = "Admin")]
        public IActionResult CreatePayment([FromBody] PaymentDto paymentDto)
        {
            try
            {
                Payment payment = new Payment
                {
                    Id = Guid.NewGuid(),
                    Name = paymentDto.Name,
                    Logo = paymentDto.Logo,
                    Status = paymentDto.Status
                };

                bool result = _paymentData.CreatePayment(payment);

                if (result)
                {
                    return StatusCode(201, payment);
                }
                else
                {
                    return StatusCode(500, "Data not inserted");
                }

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetPayments")]
        [Authorize(Roles = "Admin")]
        public IActionResult GetPayments()
        {
            try
            {
                List<Payment> payments = _paymentData.GetAll();
                return Ok(payments);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPut("EditPayment")]
        [Authorize(Roles = "Admin")]
        public IActionResult EditPayment([FromQuery] Guid id, [FromBody] PaymentDto paymentDto)
        {
            try
            {
                Payment payment = new Payment
                {
                    Id = id,
                    Name = paymentDto.Name,
                    Logo = paymentDto.Logo,
                    Status = paymentDto.Status
                };

                bool result = _paymentData.UpdatePayment(id, payment);

                if (result)
                {
                    return StatusCode(201, "Data Successfully Updated");
                }
                else
                {
                    return StatusCode(500, "Data not updated");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPut("TogleStatusPayment")]
        [Authorize(Roles = "Admin")]
        public IActionResult TogleStatusPayment([FromQuery] Guid id)
        {
            try
            {
                Payment? payment = _paymentData.CheckPayment(id);

                if (payment is null) return BadRequest("Payment Data not found");

                bool result = payment.Status ? _paymentData.DeactivatePayment(id) : _paymentData.ActivatePayment(id);
                if (result)
                {
                    return Ok("Payment Method has been activated Successfully");
                }
                return StatusCode(500, "Payment Method Has Been Deactivated Successfully");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("ResetPassword")]
        public IActionResult ResetPassword([FromBody] ResetPasswordDto resetPasswordDto)
        {
            try
            {
                User? user = _userData.CheckUser(resetPasswordDto.Email);

                if (user == null)
                {
                    return BadRequest("User Not Found");
                }
                else
                {
                    string newPass = BCrypt.Net.BCrypt.HashPassword(resetPasswordDto.Password);
                    bool succes = _userData.ResetPass(resetPasswordDto.Email, newPass);

                    if (succes)
                    {
                        return Ok("Password Has Been Changed");
                    }
                    else
                    {
                        return BadRequest("Something Went Wrong, Please Try Again");
                    }
                }


            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

    }
}
