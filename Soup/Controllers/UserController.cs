﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.IdentityModel.Tokens;
using Soup.Data;
using Soup.DTOs;
using Soup.Email;
using Soup.Email.Template;
using Soup.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;

namespace Soup.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserData _userData;
        private readonly IConfiguration _configuration;
        private readonly EmailService _emailService;
        private readonly PaymentData _paymentData;
        private readonly CartData _cartData;
        private readonly InvoiceData _invoiceData;
        private readonly CourseData _courseData;

        public UserController(UserData userData, EmailService emailService,CourseData CourseData, IConfiguration configuration, PaymentData paymentData, CartData cartData, InvoiceData invoiceData)
        {
            _userData = userData;
            _configuration = configuration;
            _courseData = CourseData;
            _emailService = emailService;
            _paymentData = paymentData;
            _cartData = cartData;
            _invoiceData = invoiceData;
        }

        //userAuth
        [HttpPost("Register")]
        public async Task<IActionResult> Register([FromBody] UserDto userDto)
        {
            try
            {
                User user = new User
                {
                    Id = Guid.NewGuid(),
                    Username = userDto.Username,
                    Email = userDto.Email,
                    Password = BCrypt.Net.BCrypt.HashPassword(userDto.Password),
                    Role = userDto.Role,
                    Status = userDto.Status
                };

                User? checkUser = _userData.CheckUser(userDto.Email);

                if(checkUser != null)
                {
                    return BadRequest("Your Email Has Been Registered");
                }
                else
                {
                bool mailResult = await SendActivationEmail(user);
                if (mailResult)
                {
                    bool result = _userData.CreateUserAccount(user);

                    if (result)
                    {
                    
                        return Ok("Email Has Ben Sent  To Your Email");
                    }
                    else
                    {
                        return StatusCode(500, "Data not inserted");
                    }
                }
                else
                {
                    return StatusCode(500, "Something Went Wrong, Please Try Again Later");
                }
            }


            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("Login")]
        public IActionResult Login([FromBody] LoginRequestDto credential)
        {
            try
            {
                if(credential is null)
                {
                    return BadRequest("Check Your Email Or Password");
                }

                if (string.IsNullOrEmpty(credential.Email) || string.IsNullOrEmpty(credential.Password))
                    return BadRequest("Check Your Email Or Password");

                User? user = _userData.CheckUser(credential.Email);

                if(user == null)
                {
                    return BadRequest("User Not Found, Please Register First");
                }

                bool isVerified = BCrypt.Net.BCrypt.Verify(credential.Password, user?.Password);

                if(user != null && !isVerified)
                {
                    return BadRequest("Incorrect Password! Please check your password!");
                }
                else
                {
                    if(user?.Status != true)
                        return Unauthorized("Your account have not activated");

                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetSection("JwtConfig:Key").Value));

                    var claims = new Claim[]
                    {
                        new Claim(ClaimTypes.Name, user.Username),
                        new Claim(ClaimTypes.Role, user.Role ? "Admin" : "User")
                    };

                    var signingCredential = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);

                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(claims),
                        Expires = DateTime.UtcNow.AddDays(1),
                        SigningCredentials = signingCredential
                    };

                    var tokenHandler = new JwtSecurityTokenHandler();
                    var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                    string token = tokenHandler.WriteToken(securityToken);

                    return Ok(new LoginResponseDto
                    {
                        Id = user.Id,
                        Username = user.Username,
                        Email = user.Email,
                        Role = user.Role,
                        Status = user.Status,
                        Token = token
                    });
                }
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("EmailResetPassword")]
        public async Task<IActionResult> SendEmailReset([FromBody] PasswordResetEmailDto passwordResetEmailDto)
        {
            try
            {
                User? user = _userData.CheckUser(passwordResetEmailDto.Email);

                List<string> to = new List<string>();
                to.Add(passwordResetEmailDto.Email);

                string Token = RemoveSpecialCharacters(user?.Password.ToString());
                var param = new Dictionary<string, string?>
            {
                {"Email", user?.Email.ToString() },
                {"Token", Token }
            };

                string subject = "Password Reset Link";
                string callBack = QueryHelpers.AddQueryString("http:http://localhost:3000/reset-password-newpass", param);
                string body = _emailService.GetEmailTemplate("EmailActivation", new EmailActivation
                {
                    Text = "To Reset Your Password. Just press the button below.",
                    Link = callBack,
                    Email = passwordResetEmailDto.Email
                });

                EmailModel emailModel = new EmailModel(to, subject, body);
                bool mailresult = await _emailService.SendAsync(emailModel, new CancellationToken());

                if (user != null)
                {
                    if (mailresult)
                    {
                        return Ok("Reset Link Has been sent to your email");
                    }
                    else
                    {
                        return BadRequest("Failed To sent email");
                    }
                }
                else
                {
                    return BadRequest("User Not Found");
                }
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("ResetPassword")]
        public IActionResult ResetPassword([FromBody] ResetPasswordDto resetPasswordDto)
        {
            try
            {
                User? user = _userData.CheckUser(resetPasswordDto.Email);

                if (user == null)
                { 
                    return BadRequest("User Not Found"); 
                }
                else
                {
                    string Token = RemoveSpecialCharacters(user?.Password.ToString());
                    bool checkToken = Token == resetPasswordDto.Token;

                    if (checkToken)
                    {
                        string newPass = BCrypt.Net.BCrypt.HashPassword(resetPasswordDto.Password);
                        bool succes = _userData.ResetPass(resetPasswordDto.Email, newPass);

                        if (succes)
                        {
                            return Ok("Password Has Been Changed");
                        }
                        else
                        {
                            return BadRequest("Something Went Wrong, Please Try Again");
                        }

                    }
                    else
                    {
                        return StatusCode(401, "Your token is not valid");
                    }
                }

                
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("ActivateUser")]
        public IActionResult ActivateUser(Guid Id, string userEmail)
        {
            try
            {
                User? user = _userData.CheckUser(userEmail);
                if (user is null) return BadRequest("User Not Found");
                if (user.Status == true)
                {
                    return BadRequest("User Has Benn Already Activated");
                }

                bool result = _userData.ActivateUser(Id);

                if (result)
                {
                    return Ok("User Activated Successfully, please go back to te website and log in");
                }
                else
                {
                    return BadRequest("Failed To Activate User");
                }

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        private async Task<bool> SendActivationEmail(User user)
        {
            List<string> to = new List<string>();
            to.Add(user.Email);

            var param = new Dictionary<string, string?>
            {
                {"Id", user.Id.ToString() },
                {"userEmail", user.Email.ToString() }
            };

            string subject = "Account Activation Email";
            string callBack = QueryHelpers.AddQueryString("http://localhost:3000/email-confirm/", param);
            string body = _emailService.GetEmailTemplate("EmailActivation", new EmailActivation
            {
                Text = "You need to confirm your account. Just press the button below.",
                Link = callBack,
                Email = user.Username
            });

            EmailModel emailModel = new EmailModel(to, subject, body);
            bool result = await _emailService.SendAsync(emailModel, new CancellationToken());

            return result;
        }
        static string RemoveSpecialCharacters(string input)
        {
            Regex regex = new Regex("[^a-zA-Z0-9]");
            return regex.Replace(input, "");
        }

        // User Course
        [HttpGet("GetAllCourse")]
        public IActionResult GetAllCourse()
        {
            try
            {
                List<Course> courses = _courseData.GetAllCourse();
                return Ok(courses);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        [HttpGet("GetCourseDetail")]
        public IActionResult GetCourseDetail(int id)
        {
            try
            {
                List<Course> courses = _courseData.GetCourseDetail(id);
                return Ok(courses);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetCourseByCategory")]
        public IActionResult GetCourseByCategory(int id)
        {
            try
            {
                List<Course> courses = _courseData.GetCourseByCategory(id);
                return Ok(courses);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }


        [HttpGet("GetAllCategories")]
        public IActionResult GetAllCategories()
        {
            try
            {
                List<Categories> categories = _courseData.GetAllCategories();
                return Ok(categories);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        [HttpGet("GetCategoriesById")]
        public IActionResult GetCategoriesById(int id)
        {
            try
            {
                Models.Categories categories = _courseData.GetCategoriesById(id);
                return Ok(categories);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        //User Payment Method
        [HttpGet("PaymentMethods")]
        [Authorize]
        public IActionResult getAllPayments()
        {
            try
            {
                List<Payment> payments = _paymentData.GetAllActivePayment();
                return Ok(payments);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // Cart Item
        [HttpPost("InsertCart")]
        [Authorize]
        public IActionResult InsertCart([FromBody] Cart cart)
        {
            try
            {

                bool result = _cartData.InsertCartItem(cart);

                if (result)
                {
                    return Ok("Success Add Items To Cart");
                }
                else
                {
                    return BadRequest("Failed Add Items To Cart");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetUserCart")]

        public IActionResult GetUserCart( Guid userId)
        {
            try
            {
                return Ok(_cartData.GetUserCart(userId));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpDelete("DeleteUserCart")]
        [Authorize]
        public IActionResult DeleteUserCart(int cartItemId)
        {
            try
            {
                bool result = _cartData.DeleteCartItem(cartItemId);
                if (result)
                {
                    return Ok("Succes Remove Item");
                }
                else
                {
                    return BadRequest("Failed To remove item");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        //Invoice User
        [HttpGet("GetAllInvoice")]
        public IActionResult GetAllInvoice()
        {
            try
            {
                List<Invoice> invoices = _invoiceData.GetAllInvoice();
                return Ok(invoices);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
        [HttpGet("GetUserInvoices")]
        public IActionResult GetUserInvoice(Guid userId)
        {
            try
            {
                List<UserInvoiceDto> invoices = _invoiceData.GetUserInvoice(userId);
                return Ok(invoices);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }
        [HttpGet("GetInvoiceItems")]
        public IActionResult GetInvoiceItems(string invoiceId)
        {
            try
            {
                List<InvoiceDetail> invoiceItems = _invoiceData.GetInvoiceItems(invoiceId);

                if (invoiceItems != null)
                {

                    return Ok(invoiceItems);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }
        [HttpPost("CreateInvoice")]
        public IActionResult CreateInvoice([FromBody] CartIDListDto cartIdListDto, Guid userId)
        {
            string? result = null;
            try
            {
                List<CartDto> cartItems = new List<CartDto>();
                foreach (var id in cartIdListDto.CartItemIds)
                {
                    cartItems.Add(_cartData.GetCartItem(id));
                }

                //CreateInvoice
                result = _invoiceData.CreateInvoice(cartItems, userId);

                //Delete Cart Item
                foreach (var id in cartIdListDto.CartItemIds)
                {
                    _cartData.DeleteCartItem(id);
                }

                if (!String.IsNullOrEmpty(result))
                {
                    return Ok($"Invoice Succesfully Created. Invoice Id: {result}");
                }
                else
                {
                    return StatusCode(500, "Failed to Create Invoice");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        //Get User Class
        [HttpGet("GetUserClass")]
        public IActionResult GetUserClass(Guid id)
        {
            try
            {
                List<MyClassDto> myClasses = _userData.GetUserClass(id);
                if(myClasses is DBNull)
                {
                    return StatusCode(404,"No Class Yet");
                    
                }
                else
                {
                    return Ok(myClasses);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
