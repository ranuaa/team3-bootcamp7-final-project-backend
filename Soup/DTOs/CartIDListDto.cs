﻿namespace Soup.DTOs
{
    public class CartIDListDto
    {
        public List<int> CartItemIds { get; set; }
    }
}
