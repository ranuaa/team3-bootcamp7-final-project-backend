﻿namespace Soup.DTOs
{
    public class LoginResponseDto
    {
        public Guid Id { get; set; }
        public string? Username { get; set; } = string.Empty;
        public string? Email { get; set; } = string.Empty;
        public bool Role { get; set; }
        public bool Status { get; set; }
        public string? Token { get; set; } = string.Empty;
    }
}
