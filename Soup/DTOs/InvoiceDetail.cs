﻿namespace Soup.DTOs
{
    public class InvoiceDetail
    {

        public string Name { get; set; } = string.Empty;
        public string Category { get; set; } = string.Empty;
        public int Price { get; set; }
        public DateTime BookingDate { get; set; }
    }
}
