﻿namespace Soup.DTOs
{
    public class InsertCartDto
    {
        public Guid UserId { get; set; }
        public int CourseId { get; set; }
        public DateTime BookingDate { get; set; }
        public int BookingPrice { get; set; }
    }
}
