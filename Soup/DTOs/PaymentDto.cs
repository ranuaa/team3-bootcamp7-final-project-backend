﻿namespace Soup.DTOs
{
    public class PaymentDto
    {
        public string Name { get; set; } = string.Empty;
        public string Logo { get; set; } = string.Empty;
        public bool Status { get; set; }
    }
}
