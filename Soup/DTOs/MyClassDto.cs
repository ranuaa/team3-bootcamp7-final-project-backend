﻿namespace Soup.DTOs
{
    public class MyClassDto
    {
        public string? Name { get; set; } = string.Empty;
        public string Category { get; set; } = string.Empty;
        public string? Image { get; set; } = string.Empty;
        public DateTime? Schedule { get; set; }
    }
}
