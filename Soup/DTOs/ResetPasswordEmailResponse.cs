﻿namespace Soup.DTOs
{
    public class ResetPasswordEmailResponse
    {
        public string? Email { get; set; } = string.Empty;
        public string? ResetToken { get; set; } = string.Empty;
    }
}
