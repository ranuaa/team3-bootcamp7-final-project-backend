﻿namespace Soup.DTOs
{
    public class PasswordResetEmailDto
    {
        public string Email { get; set; } = string.Empty;
    }
}
