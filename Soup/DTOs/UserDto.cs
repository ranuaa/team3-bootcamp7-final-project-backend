﻿namespace Soup.DTOs
{
    public class UserDto
    {
        public string Username { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public bool Role { get; set; }
        public bool Status { get; set; }
    }
}
