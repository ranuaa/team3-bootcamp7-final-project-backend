﻿namespace Soup.DTOs
{
    public class UserInvoiceDto
    {
        public string InvoiceId { get; set; } = string.Empty;
        public DateTime OrderDate { get; set; }
        public int TotalCourse { get; set; }
        public int TotalPrice { get; set; }
    }
}
