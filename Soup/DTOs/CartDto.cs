﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Soup.DTOs
{
    public class CartDto
    {
        public int? CartItemId { get; set; }
        public int CourseId { get; set; }
        public string Category { get; set; } = string.Empty;
        public string CourseName { get; set; } = string.Empty;
        public string Image { get; set; } = string.Empty;
        public DateTime BookingDate { get; set; }
        public int BookingPrice { get; set; }

    }
}
