﻿using System.Data.SqlClient;
using Soup.DTOs;
using Soup.Models;

namespace Soup.Data
{
    public class CartData
    {
        private readonly string _connectionString;
        private readonly IConfiguration _configuration;

        public CartData(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        public bool InsertCartItem(Cart cart)
        {
            bool result = false;

            using(SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.Transaction = transaction;
                    command.CommandText = "INSERT INTO cart_items(fk_user_id,fk_course_id,booking_date,booking_price) VALUES(@fk_user_id,@fk_course_id,@booking_date,@booking_price)";

                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@fk_user_id", cart.UserId);
                    command.Parameters.AddWithValue("@fk_course_id", cart.CourseId);
                    command.Parameters.AddWithValue("@booking_date", cart.BookingDate);
                    command.Parameters.AddWithValue("@booking_price", cart.BookingPrice);

                    result = command.ExecuteNonQuery() > 0 ? true : false;
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }

        public List<CartDto> GetUserCart(Guid userId)
        {
            List<CartDto> carts = new List<CartDto>();

            using(SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.Transaction = transaction;
                    command.Parameters.Clear();

                    command.CommandText = "SELECT cart_items.cart_item_id ,courses.course_id, categories.name as category ,courses.name, cart_items.booking_date, courses.price, courses.image   FROM cart_items INNER JOIN courses ON cart_items.fk_course_id = courses.course_id INNER JOIN categories ON courses.fk_category_id = categories.category_id WHERE fk_user_id = @userId ";
                    command.Parameters.AddWithValue("userId", userId);

                    using(SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            carts.Add(new CartDto
                            {
                                CartItemId = Convert.ToInt32(reader["cart_item_id"]),
                                CourseId = Convert.ToInt32(reader["course_id"]),
                                Category = reader["category"].ToString() ?? string.Empty,
                                CourseName = reader["name"].ToString() ?? string.Empty,
                                Image = reader["image"].ToString() ?? string.Empty,
                                BookingDate = Convert.ToDateTime(reader["booking_date"]),
                                BookingPrice = Convert.ToInt32(reader["price"]),
                            });
                        }
                    }
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

            return carts;
        }
        public CartDto GetCartItem(int cartItemId)
        {
            CartDto? cartItem = null;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.Transaction = transaction;
                    command.Parameters.Clear();

                    command.CommandText = $"SELECT * FROM cart_items WHERE cart_item_id = @cartItemId";

                    command.Parameters.AddWithValue("@cartItemId", cartItemId);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            cartItem = new CartDto
                            {
                                CartItemId = Convert.ToInt32(reader["cart_item_id"]),
                                CourseId = Convert.ToInt32(reader["fk_course_id"]),
                                BookingDate = Convert.ToDateTime(reader["booking_date"]),
                                BookingPrice = Convert.ToInt32(reader["booking_price"])
                            };
                        }
                    }
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

            return cartItem;
        }

        public bool DeleteCartItem(int cartItemId)
        {
            bool result = false;

            using(SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.Transaction = transaction;
                    command.Parameters.Clear();

                    command.CommandText = "DELETE FROM cart_items WHERE cart_item_id = @id";
                    command.Parameters.AddWithValue("@id", cartItemId);

                    result = command.ExecuteNonQuery() > 0 ? true : false;
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }


    }
}
