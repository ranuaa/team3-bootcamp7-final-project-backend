using System.Data.SqlClient;
using Soup.Models;

namespace Soup.Data
{
    public class CourseData
    {
        private readonly string _connectionString;
        private readonly IConfiguration _configuration;

        public CourseData(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        public List<Course> GetAllCourse()
        {
            List<Course> courses = new List<Course>();

            string query =
                "SELECT c.course_id, c.name, c.price, c.description, c.image, c.fk_category_id as id_categories, t.name as categories " +
                "FROM courses c " +
                "LEFT JOIN categories t ON t.category_id = c.fk_category_id;";

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                courses.Add(new Course
                                {
                                    Id = Convert.ToInt32(reader["course_id"]),
                                    Name = reader["name"].ToString() ?? string.Empty,
                                    Price = Convert.ToInt32(reader["price"]),
                                    Description = reader["description"].ToString() ?? string.Empty,
                                    Image = reader["image"].ToString() ?? string.Empty,
                                    Id_categories = Convert.ToInt32(reader["id_categories"]),
                                    Categories = reader["categories"].ToString() ?? string.Empty,
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }

                }
            }

            return courses;
        }
        public List<Course> GetCourseDetail(int id)
        {
            List<Course> courses = new List<Course>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "SELECT courses.course_id, courses.name, courses.description,courses.image, courses.price, courses.fk_category_id, categories.name as category FROM courses INNER JOIN categories On courses.fk_category_id = categories.category_id WHERE courses.course_id = @id";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@id", id);
                    try
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                courses.Add(new Course
                                {
                                    Id = Convert.ToInt32(reader["course_id"]),
                                    Name = reader["name"].ToString() ?? string.Empty,
                                    Price = Convert.ToInt32(reader["price"]),
                                    Description = reader["description"].ToString() ?? string.Empty,
                                    Image = reader["image"].ToString() ?? string.Empty,
                                    Id_categories = Convert.ToInt32(reader["fk_category_id"]),
                                    Categories = reader["category"].ToString() ?? string.Empty,
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }

                }
            }

            return courses;
        }
        public List<Course> GetCourseByCategory(int id)
        {
            List<Course> courses = new List<Course>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();

                    command.Connection = connection;
                    command.Parameters.Clear();

                    command.CommandText =
                        "SELECT c.course_id, c.name, c.price, c.description, c.image, c.fk_category_id as id_categories, t.name as categories " +
                        "FROM courses c JOIN categories t ON t.category_id = c.fk_category_id WHERE c.fk_category_id = @id";


                    command.Parameters.AddWithValue("@id", id);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            courses.Add(new Course
                            {
                                Id = Convert.ToInt32(reader["course_id"]),
                                Name = reader["name"].ToString() ?? string.Empty,
                                Price = Convert.ToInt32(reader["price"]),
                                Description = reader["description"].ToString() ?? string.Empty,
                                Image = reader["image"].ToString() ?? string.Empty,
                                Id_categories = Convert.ToInt32(reader["id_categories"]),
                                Categories = reader["categories"].ToString() ?? string.Empty,
                            });
                        }
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
            return courses;
        }
        public List<Models.Categories> GetAllCategories()
        {
            List<Models.Categories> categories = new List<Models.Categories>();

            string query = "SELECT * FROM categories";

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                categories.Add(new Models.Categories
                                {
                                    Id = Convert.ToInt32(reader["category_id"]),
                                    Name = reader["name"].ToString() ?? string.Empty,
                                    Description = reader["description"].ToString() ?? string.Empty,
                                    ImageBanner = reader["img_banner"].ToString() ?? string.Empty,
                                    ImageIcon = reader["img_icon"].ToString() ?? string.Empty,
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }

                }
            }

            return categories;
        }
        public Categories? GetCategoriesById(int id)
        {
            Categories? categories = null;
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "SELECT * FROM Categories WHERE category_id = @id";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@id", id);

                    try
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                categories = new Categories
                                {
                                    Id = Convert.ToInt32(reader["category_id"]),
                                    Name = reader["name"].ToString() ?? string.Empty,
                                    Description = reader["description"].ToString() ?? string.Empty,
                                    ImageBanner = reader["img_banner"].ToString() ?? string.Empty,
                                    ImageIcon = reader["img_icon"].ToString() ?? string.Empty,
                                };
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }

                }
            }
            return categories;
        }
    }
}
