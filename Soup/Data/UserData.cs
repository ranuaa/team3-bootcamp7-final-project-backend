﻿using System.Data.SqlClient;
using Soup.DTOs;
using Soup.Models;

namespace Soup.Data
{
    public class UserData
    {
        private readonly string _connectionString;
        private readonly IConfiguration _configuration;

        public UserData(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        public bool CreateUserAccount(User user)
        {
            bool result = false;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.Transaction = transaction;
                    command.Parameters.Clear();

                    command.CommandText = "INSERT INTO users VALUES(@user_id, @name, @email, @password, @role, @status)";
                    command.Parameters.AddWithValue("@user_id", user.Id);
                    command.Parameters.AddWithValue("@name", user.Username);
                    command.Parameters.AddWithValue("@email", user.Email);
                    command.Parameters.AddWithValue("@password", user.Password);
                    command.Parameters.AddWithValue("@role", user.Role);
                    command.Parameters.AddWithValue("@status", user.Status);

                    result = command.ExecuteNonQuery() > 0 ? true : false;

                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }

            }

                return result;
        }

        public bool UpdateUserAccount(Guid id, User user)
        {
            bool result = false;

            using(SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.Transaction = transaction;
                    command.Parameters.Clear();

                    command.CommandText = $"UPDATE users SET name = @name, email = @email, role = @role WHERE user_id = @id";

                    command.Parameters.AddWithValue("@id", id);
                    command.Parameters.AddWithValue("@name", user.Username);
                    command.Parameters.AddWithValue("@email", user.Email);
                    command.Parameters.AddWithValue("@role", user.Role);

                    result = command.ExecuteNonQuery() > 0 ? true : false;

                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }

        public List<User> GetAll()
        {
            List<User> users = new List<User>();
            string query = "SELECT * FROM users";
            using(SqlConnection connection = new SqlConnection(_connectionString))
            {
                using(SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                users.Add(new User
                                {
                                    Id = Guid.Parse(reader["user_id"].ToString() ?? string.Empty),
                                    Username = reader["name"].ToString() ?? string.Empty,
                                    Email = reader["email"].ToString() ?? string.Empty,
                                    Role = Convert.ToBoolean(reader["role"]),
                                    Status = Convert.ToBoolean(reader["status"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return users;
        }

        public List<User> GetAllUserFromName()
        {
            List<User> users = new List<User>();
            string query = "SELECT * FROM users ORDER BY name  ASC";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                users.Add(new User
                                {
                                    Id = Guid.Parse(reader["user_id"].ToString() ?? string.Empty),
                                    Username = reader["name"].ToString() ?? string.Empty,
                                    Email = reader["email"].ToString() ?? string.Empty,
                                    Role = Convert.ToBoolean(reader["role"]),
                                    Status = Convert.ToBoolean(reader["status"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return users;
        }

        public List<User> GetAllUserFromEmail()
        {
            List<User> users = new List<User>();
            string query = "SELECT * FROM users ORDER BY email  ASC";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                users.Add(new User
                                {
                                    Id = Guid.Parse(reader["user_id"].ToString() ?? string.Empty),
                                    Username = reader["name"].ToString() ?? string.Empty,
                                    Email = reader["email"].ToString() ?? string.Empty,
                                    Role = Convert.ToBoolean(reader["role"]),
                                    Status = Convert.ToBoolean(reader["status"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return users;
        }

        public List<User> GetAllUserFromRole()
        {
            List<User> users = new List<User>();
            string query = "SELECT * FROM users ORDER BY role  ASC";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                users.Add(new User
                                {
                                    Id = Guid.Parse(reader["user_id"].ToString() ?? string.Empty),
                                    Username = reader["name"].ToString() ?? string.Empty,
                                    Email = reader["email"].ToString() ?? string.Empty,
                                    Role = Convert.ToBoolean(reader["role"]),
                                    Status = Convert.ToBoolean(reader["status"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return users;
        }

        public List<User> GetAllUserByStatus()
        {
            List<User> users = new List<User>();
            string query = "SELECT * FROM users ORDER BY status  ASC";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                users.Add(new User
                                {
                                    Id = Guid.Parse(reader["user_id"].ToString() ?? string.Empty),
                                    Username = reader["name"].ToString() ?? string.Empty,
                                    Email = reader["email"].ToString() ?? string.Empty,
                                    Role = Convert.ToBoolean(reader["role"]),
                                    Status = Convert.ToBoolean(reader["status"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return users;
        }

        public User? CheckUser(string email)
        {
            User? user = null;

            using(SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "SELECT * FROM users WHERE email = @email";
                    command.Parameters.Clear();

                    command.Parameters.AddWithValue("@email", email);
                    connection.Open();

                    using(SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            user = new User
                            {
                                Id = Guid.Parse(reader["user_id"].ToString() ?? string.Empty),
                                Username = reader["name"].ToString() ?? string.Empty,
                                Password = reader["password"].ToString() ?? string.Empty,
                                Email = reader["email"].ToString() ?? string.Empty,
                                Role = Convert.ToBoolean(reader["role"]),
                                Status = Convert.ToBoolean(reader["status"])
                            };
                        }
                    }
                    connection.Close();
                }
            }

            return user;
        }

        public bool ActivateUser(Guid id)
        {
            bool result = false;

            using(SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = new SqlCommand();

                command.Connection = connection;
                command.Parameters.Clear();
                command.CommandText = " UPDATE users SET status = 1 WHERE user_id = @id ";

                command.Parameters.AddWithValue("@id", id);

                try
                {
                    connection.Open();
                    result = command.ExecuteNonQuery() > 0 ? true : false;
                }
                catch
                {
                    throw;
                }
                finally
                {
                    connection.Close();
                }

            }

            return result;
        }

        public bool DeactivateUser(Guid id)
        {
            bool result = false;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = new SqlCommand();

                command.Connection = connection;
                command.Parameters.Clear();
                command.CommandText = " UPDATE users SET status = 0 WHERE user_id = @id ";

                command.Parameters.AddWithValue("@id", id);

                try
                {
                    connection.Open();
                    result = command.ExecuteNonQuery() > 0 ? true : false;
                }
                catch
                {
                    throw;
                }
                finally
                {
                    connection.Close();
                }

            }

            return result;
        }

        public bool ResetPass(string Email, string Password)
        {
            bool result = false;

            using(SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = new SqlCommand();

                command.Connection = connection;
                command.Parameters.Clear();
                command.CommandText = "UPDATE users SET password = @Password WHERE email = @Email";
                command.Parameters.AddWithValue("@Email", Email);
                command.Parameters.AddWithValue("@Password", Password);

                try
                {
                    connection.Open();
                    result = command.ExecuteNonQuery() > 0 ? true : false;
                }
                catch
                {
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }

        public List<MyClassDto> GetUserClass(Guid id)
        {
            List<MyClassDto> myClassDtos = new List<MyClassDto>();
            using(SqlConnection connection = new SqlConnection(_connectionString))
            {
                using(SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "SELECT courses.image, categories.name as Category , courses.name,invoice_items.booking_date  FROM users  LEFT JOIN invoices ON users.user_id = invoices.fk_user_id LEFT JOIN invoice_items ON invoices.invoice_id = invoice_items.fk_invoice_id LEFT JOIN courses ON invoice_items.fk_course_id = courses.course_id LEFT JOIN categories ON courses.fk_category_id = categories.category_id WHERE users.user_id =@id";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@id", id);
                    try
                    {
                        connection.Open();
                        using(SqlDataReader reader = command.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                myClassDtos.Add(new MyClassDto
                                {
                                    Image = reader["image"].ToString() ?? string.Empty,
                                    Category = reader["Category"].ToString() ?? string.Empty,
                                    Name = reader["name"].ToString() ?? string.Empty,
                                    Schedule = Convert.ToDateTime(reader["booking_date"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return myClassDtos;
        }

    }
}
