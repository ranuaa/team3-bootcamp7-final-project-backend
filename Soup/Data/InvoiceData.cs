﻿using System.Data.SqlClient;
using Soup.DTOs;
using Soup.Models;

namespace Soup.Data
{
    public class InvoiceData
    {
        private readonly string _connectionString;
        private readonly IConfiguration _configuration;

        public InvoiceData(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public int GetNextInvoiceNumber()
        {
            string query = "SELECT COUNT(*) FROM invoices";
            int count = 0;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = new SqlCommand(query, connection);
                connection.Open();
                count = (int)command.ExecuteScalar();
            }

            return count + 1;
        }
        public string GenerateInvoiceId()
        {
            string prefix = "SOU";
            int nextNumber = GetNextInvoiceNumber();
            string invoiceNumber = nextNumber.ToString("D6");
            string invoiceId = prefix + invoiceNumber;
            return invoiceId;
        }
        public string? CreateInvoice(List<CartDto> cartDtos, Guid userId)
        {
            string? invoiceId = null;

            using(SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    //Generate Invoice Id 
                    invoiceId = GenerateInvoiceId();

                    //add to invoice items
                    int totalCourse = 0;
                    int totalPrice = 0;

                    foreach(var item in cartDtos)
                    {
                        totalPrice += item.BookingPrice;
                        totalCourse++;

                        SqlCommand command1 = new SqlCommand();
                        command1.Connection = connection;
                        command1.Transaction = transaction;
                        command1.Parameters.Clear();

                        command1.CommandText = "INSERT INTO invoice_items (fk_course_id,booking_date,booking_price, fk_invoice_id) VALUES(@courseId, @bookingDate, @bookingPrice, @invoiceId)";
                        command1.Parameters.AddWithValue("@courseId", item.CourseId);
                        command1.Parameters.AddWithValue("@bookingDate", item.BookingDate);
                        command1.Parameters.AddWithValue("@bookingPrice", item.BookingPrice);
                        command1.Parameters.AddWithValue("@invoiceId", invoiceId);

                        command1.ExecuteNonQuery();
                    }

                    //Create Invoice

                    SqlCommand command2 = new SqlCommand();
                    command2.Connection = connection;
                    command2.Transaction = transaction;
                    command2.Parameters.Clear();

                    command2.CommandText = "INSERT INTO invoices (invoice_id, order_date, fk_user_id, total_course, total_price) VALUES (@invoiceId, @orderDate, @userId, @totalCourse, @totalPrice)";

                    command2.Parameters.AddWithValue("@invoiceId", invoiceId);
                    command2.Parameters.AddWithValue("@orderDate", DateTime.Now);
                    command2.Parameters.AddWithValue("@userId", userId);
                    command2.Parameters.AddWithValue("@totalCourse", totalCourse);
                    command2.Parameters.AddWithValue("@totalPrice", totalPrice);

                    var result = command2.ExecuteNonQuery() > 0 ? true : false;

                    transaction.Commit();

                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }

            }

            return invoiceId;
        }
        public List<Invoice>? GetAllInvoice()
        {
            List<Invoice>? invoices = new List<Invoice>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "SELECT * FROM invoices";
                    command.Parameters.Clear();

                    try
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                invoices.Add(new Invoice
                                {
                                    InvoiceId = reader["invoice_id"].ToString() ?? string.Empty,
                                    UserId = Guid.Parse(reader["fk_user_id"].ToString() ?? string.Empty),
                                    OrderDate = Convert.ToDateTime(reader["order_date"]),
                                    TotalCourse = Convert.ToInt32(reader["total_course"]),
                                    TotalPrice = Convert.ToInt32(reader["total_price"]),
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }

                }
            }
            return invoices;
        }
        public List<UserInvoiceDto>? GetUserInvoice(Guid id)
        {
            List<UserInvoiceDto>? userInvoicesDto = new List<UserInvoiceDto>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "SELECT * FROM invoices WHERE fk_user_id = @id";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@id", id);

                    try
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                userInvoicesDto.Add(new UserInvoiceDto
                                {
                                    InvoiceId = reader["invoice_id"].ToString() ?? string.Empty,
                                    OrderDate = Convert.ToDateTime(reader["order_date"]),
                                    TotalCourse = Convert.ToInt32(reader["total_course"]),
                                    TotalPrice = Convert.ToInt32(reader["total_price"]),
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }

                }
            }
            return userInvoicesDto;
        }
        public List<InvoiceDetail>? GetInvoiceItems(String invoiceId)
        {
            List<InvoiceDetail>? invoiceItems = new List<InvoiceDetail>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.Parameters.Clear();
                    command.CommandText = "SELECT courses.name,categories.name as Category,  courses.price, invoice_items.booking_date FROM invoice_items INNER JOIN courses ON invoice_items.fk_course_id = courses.course_id INNER JOIN invoices ON invoice_items.fk_invoice_id = invoices.invoice_id INNER JOIN categories ON courses.fk_category_id = categories.category_id WHERE invoice_id = @invoiceId";
                    command.Parameters.AddWithValue("@invoiceId", invoiceId);

                    try
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                invoiceItems.Add(new InvoiceDetail
                                {
                                    Name = reader["name"].ToString() ?? string.Empty,
                                    Category = reader["Category"].ToString() ?? string.Empty,
                                    BookingDate = Convert.ToDateTime(reader["booking_date"]),
                                    Price = Convert.ToInt32(reader["price"]),
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }

                }
            }
            return invoiceItems;
        }

    }
}
