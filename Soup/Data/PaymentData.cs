﻿using System.Data.SqlClient;
using Soup.Models;

namespace Soup.Data
{
    public class PaymentData
    {
        private readonly string _connectionstring;
        private readonly IConfiguration _configuration;

        public PaymentData(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionstring = _configuration.GetConnectionString("DefaultConnection");
        }

        public bool CreatePayment(Payment payment)
        {
            bool result = false;

            using(SqlConnection connection = new SqlConnection(_connectionstring))
            {
                connection.Open();

                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.Transaction = transaction;

                    command.Parameters.Clear();

                    command.CommandText = "INSERT INTO payment_methods VALUES (@payment_id, @name, @logo, @status)";

                    command.Parameters.AddWithValue("@payment_id", payment.Id);
                    command.Parameters.AddWithValue("@name", payment.Name);
                    command.Parameters.AddWithValue("@logo", payment.Logo);
                    command.Parameters.AddWithValue("@status", payment.Status);

                    result = command.ExecuteNonQuery() > 0 ? true : false;
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }

        public List<Payment> GetAll()
        {
            List<Payment> payments = new List<Payment>();

            string query = "SELECT * FROM payment_methods";

            using(SqlConnection connection = new SqlConnection(_connectionstring))
            {
                using(SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        using(SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                payments.Add(new Payment
                                {
                                    Id = Guid.Parse(reader["payment_id"].ToString() ?? string.Empty),
                                    Name = reader["name"].ToString() ?? string.Empty,
                                    Logo = reader["logo"].ToString() ?? string.Empty,
                                    Status = Convert.ToBoolean(reader["status"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return payments;
        }

        public List<Payment> GetAllActivePayment()
        {
            List<Payment> payments = new List<Payment>();

            string query = "SELECT * FROM payment_methods WHERE status = 1";

            using (SqlConnection connection = new SqlConnection(_connectionstring))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                payments.Add(new Payment
                                {
                                    Id = Guid.Parse(reader["payment_id"].ToString() ?? string.Empty),
                                    Name = reader["name"].ToString() ?? string.Empty,
                                    Logo = reader["logo"].ToString() ?? string.Empty,
                                    Status = Convert.ToBoolean(reader["status"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return payments;
        }

        public bool UpdatePayment(Guid id, Payment payment)
        {
            bool result = false;

            using(SqlConnection connection = new SqlConnection(_connectionstring))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.Transaction = transaction;
                    command.Parameters.Clear();

                    command.CommandText = $"UPDATE payment_methods SET name = @name, logo = @logo, status = @status WHERE payment_id = @id";

                    command.Parameters.AddWithValue("@id", id);
                    command.Parameters.AddWithValue("@name", payment.Name);
                    command.Parameters.AddWithValue("@logo", payment.Logo);
                    command.Parameters.AddWithValue("@status", payment.Status);

                    result = command.ExecuteNonQuery() > 0 ? true : false;
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }

        public bool ActivatePayment(Guid id)
        {
            bool result = false;

            using (SqlConnection connection = new SqlConnection(_connectionstring))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.Transaction = transaction;
                    command.Parameters.Clear();

                    command.CommandText = "UPDATE payment_methods SET status = 1 WHERE payment_id = @id";

                    command.Parameters.AddWithValue("@id", id);
                    result = command.ExecuteNonQuery() > 0 ? true : false;
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;

        }

        public bool DeactivatePayment(Guid id)
        {
            bool result = false;

            using (SqlConnection connection = new SqlConnection(_connectionstring))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.Transaction = transaction;
                    command.Parameters.Clear();

                    command.CommandText = "UPDATE payment_methods SET status = 0 WHERE payment_id = @id";

                    command.Parameters.AddWithValue("@id", id);
                    result = command.ExecuteNonQuery() > 0 ? true : false;
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;

        }

        public Payment? CheckPayment(Guid id)
        {
            Payment? payment = null;

            using (SqlConnection connection = new SqlConnection(_connectionstring))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "SELECT status From payment_methods WHERE payment_id = @id";

                    command.Parameters.Clear();

                    command.Parameters.AddWithValue("@id", id);

                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            payment = new Payment
                            {
                                Status = Convert.ToBoolean(reader["status"])
                            };
                        }
                    }
                    connection.Close();

                }
            }

            return payment;
        }
    }
}
