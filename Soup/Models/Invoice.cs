﻿namespace Soup.Models
{
    public class Invoice
    {
        public string InvoiceId { get; set; } = string.Empty;
        public Guid? UserId { get; set; }
        public DateTime OrderDate { get; set; }
        public int TotalCourse { get; set; }
        public int TotalPrice { get; set; }
    }
}
