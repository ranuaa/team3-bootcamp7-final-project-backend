﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Soup.Models
{
    public class Cart
    {
        public Guid UserId { get; set; }
        public int CourseId { get; set; }
        public string? Name { get; set; } = string.Empty;
        public DateTime BookingDate { get; set; }
        public int BookingPrice { get; set; }

    }
}
