namespace Soup.Models
{
	public class Categories
	{
		public int Id { get; set; }
		public string Name { get; set; } = string.Empty;
		public string Description { get; set; } = string.Empty;
		public string ImageBanner { get; set; } = string.Empty;
		public string ImageIcon { get; set; } = string.Empty;
	}
}
